# Best Case Scenarios for Copyright — graphic assets

This repository contains the source files for the graphic identity of the 
[Best Case Scenarios for Copyright](http://www.communia-association.org/bcs-copyright) campaign,
designed by [Manufactura Independente](http://manufacturaindependente.org).


**Best Case Scenarios for Copyright** is an initiative by [COMMUNIA]
(http://www.communia-association.org), presenting best examples of copyright 
exceptions and limitations found in national laws of member states 
of the European Union.


### Folder Structure

* `00 - Intro Image` folder contains the files used to produce the introductory 
image for the campaign.
* `01 - Panorama`, `02 - Parody`, `03 - Education` and `04 - Quotations` folders 
contain the files used to produce the materials for each Best Case Scenario. 
In each case you'll find the corresponding editable files, and exports, for:
  * `X Factsheet.svg`, editable factsheet. A4, double side composition 
  (each side is composed in a layer; turn on/off layer visibility to edit). 
  There are PDF and PNG versions of the factsheet as well.
  * `X Report.odt` and `X Report.pdf` contains the layout for the individual 
  reports.
  * `X Social Media.svg`, editable graphic composition for the image used in 
  social media networks.
* `05 - Poster` contains the files used to produce an A3 poster for the whole 
Best Case Scenario Campaign.
* `Common Elements` contains the bitmap images used across the campaign 
and the campaign header/logo (BCS-header.svg). 
Image credits and links to the original files can be found below.
* `Typefaces` contains the fonts used in the campaign. 
All credits and license information are described below. 


### Image Credits

**Panorama in Portugal** uses a composition of parts from two images, both under 
a Public Domain license:

* Building, from [Broadway, East Side. 37th to 40th St.](http://digitalcollections.nypl.org/items/510d47e4-4241-a3d9-e040-e00a18064a99) painting from (1899).  
* Painter, from page 106 of [Paris Herself Again in 1878-9 ... With ... illustrations ... Fourth edition](https://www.flickr.com/photos/12403504@N02/11137962884), (1880).


**Parody in France** is based on [Gentleman Holding a Crop](https://archive.org/details/mma_gentleman_holding_a_crop_387247), a print in the 
Public Domain (1629).


**Education in Estonia** is based on an image from [page 86 of The Bookshelf 
for boys and girls Children's Book of Fact and Fancy](https://www.flickr.com/photos/internetarchivebookimages/14773043312), under the Public Domain 
(1912).  

**Quotations in Finland** is based on the [Two Women Reclining on the Floor of a Room 
and Reading a Book](https://archive.org/details/mma_two_women_reclining_on_the_floor_of_a_room_and_reading_a_book_56985) print (1730).  


### Typefaces

* **Merriweather Sans & Serif**, a typeface designed by [Eben Sorkin](http://sorkintype.com), 
under the [SIL Open Font License](http://scripts.sil.org/OFL).
* **Reglo Bold**, a typeface designed by [Sebastien Sanfilippo](http://love-letters.be), 
under the [SIL Open Font License](http://scripts.sil.org/OFL).
* **Roboto**, a typeface designed by [Christian Robertson](http://www.christianrobertson.com), 
under the [Apache License](http://www.apache.org/licenses/LICENSE-2.0.html)
